FROM golang:1.6.2-alpine

EXPOSE 8080

ENV CONSUL_ADDR=127.0.0.1:8500 PORT=8080

RUN apk add --no-cache git \
 && go get -v github.com/gorilla/mux \
              github.com/hashicorp/consul/api

COPY . /go/src/ab-api

RUN go install ab-api

CMD [ "/go/bin/ab-api" ]
