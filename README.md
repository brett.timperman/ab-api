## A/B testing API

Golang API for adding A/B test rules to Consul. [Reference app](https://gitlab.com/brett.timperman/ab-reference)

### Start
```
docker run -d -p 8080:8080 -e CONSUL_ADDR=consul:8500 btimperman/ab-api
curl localhost:8080/api/splits
```
