package pairs

import (
  "log"
  "sort"
)

type Pair struct {
  Key   string
  Value int
}

type Pairs []Pair

func (p Pairs) Swap(i, j int) { p[i], p[j] = p[j], p[i] }
func (p Pairs) Len() int { return len(p) }
func (p Pairs) Less(i, j int) bool { return p[i].Value < p[j].Value }

func New() (Pairs) {
  return make(Pairs, 0, 0)
}

func (p Pairs) ToMap(defaultTag string, tags []string) (map[string]int) {
  sort.Sort(p)
  m := make(map[string]int)
  sum := 0
  for _, pair := range p {
    log.Printf("pair key=%s, value=%v", pair.Key, pair.Value)
    m[pair.Key] = pair.Value - sum
    log.Printf("map key=%s, value=%v", pair.Key, m[pair.Key])
    sum += m[pair.Key]
    log.Printf("sum=%v", sum)
  }
  if sum < 100 {
    m[defaultTag] = (100 - sum)
  }
  for _, tag := range tags {
    if _, ok := m[tag]; !ok {
      log.Printf("setting tag=%s to 0", tag)
      m[tag] = 0
    }
  }
  return m
}

func SortedFromMap(m map[string]int) (Pairs) {
  p := New()
  for k, v := range m {
    p = append(p, Pair{ k, v })
  }
  sort.Sort(p)
  return p
}
