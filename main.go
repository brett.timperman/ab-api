package main

import (
    "fmt"
    "encoding/json"
    "log"
    "net/http"
    "os"

    "ab-api/consul"
    "github.com/gorilla/mux"
)

type handler func(w http.ResponseWriter, r *http.Request)

func main() {
  c := consul.New(os.Getenv("CONSUL_ADDR"))

  router := mux.NewRouter().StrictSlash(true)
  router.HandleFunc("/api/health", Health(c))
  router.HandleFunc("/api/splits", Splits(c)).Methods("GET")
  router.HandleFunc("/api/splits", UpdateSplits(c)).Methods("POST")

  port := os.Getenv("PORT")
  log.Printf("Listening on %s", port)
  log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), router))
}

func Health(c *consul.Consul) (handler) {
  return func(w http.ResponseWriter, r *http.Request){
    result := make(map[string]string)
    leader, err := c.Client.Status().Leader()
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }
    result["leader"] = leader
    json.NewEncoder(w).Encode(result)
  }
}

func Splits(c *consul.Consul) (handler) {
  return func(w http.ResponseWriter, r *http.Request){
    json.NewEncoder(w).Encode(c.GetSplits())
  }
}

func UpdateSplits(c *consul.Consul) (handler) {
  return func(w http.ResponseWriter, r *http.Request){
    splits := make(map[string]*consul.Split)
    decoder := json.NewDecoder(r.Body)
    err := decoder.Decode(&splits)
    if err != nil {
        http.Error(w, err.Error(), http.StatusInternalServerError)
        return
    }
    c.UpdateSplits(splits)
    json.NewEncoder(w).Encode(splits)
  }
}
