package consul

import (
    "fmt"
    "log"
    "strconv"
    "strings"

    "ab-api/pairs"
    "github.com/hashicorp/consul/api"
)

type Consul struct {
  Client *api.Client
}

type Split struct {
  Hash     string
  Services []*api.CatalogService
  Spread   map[string]int
}

func New(address string) (*Consul) {
  config := api.DefaultConfig()
  config.Address = address
  client, err := api.NewClient(config)
  if err != nil {
    panic(err)
  }

  consul := &Consul{
    Client: client,
  }

  return consul
}

func (c *Consul) getHash(service string) (string) {
  hash := c.getSplitKey(service, "hash")
  if hash == "" {
    hash = "$msec"
  }
  return hash
}

func (c *Consul) getSplitKey(service, key string) (string) {
  kv := c.Client.KV()
  pair, _, err := kv.Get(fmt.Sprintf("split/%s/%s", service, key), nil)
  if err != nil {
    panic(err)
  }
  if pair != nil {
    return fmt.Sprintf("%s", pair.Value)
  }
  return ""
}

func (c *Consul) getSplitSpread(service, defaultTag string, tags []string) (map[string]int) {
  kv := c.Client.KV()
  keys, _, err := kv.Keys(fmt.Sprintf("split/%s/values/", service), "", nil)
  if err != nil {
    panic(err)
  }
  if keys != nil {
    p := pairs.New()
    for _, k := range keys {
      pair, _, err := kv.Get(k, nil)
      if err != nil {
        panic(err)
      } else {
        index := strings.LastIndex(k, "/") + 1
        if percent, err := strconv.Atoi(k[index:len(k)]); err == nil {
          p = append(p, pairs.Pair{fmt.Sprintf("%s", pair.Value), percent})
        }
      }
    }
    return p.ToMap(defaultTag, tags)
  }

  spread := make(map[string]int)
  for _, tag := range tags {
    spread[tag] = 0
  }
  if defaultTag != "" {
    spread[defaultTag] = 100
  }
  return spread
}

func (c *Consul) GetSplits() (map[string]*Split) {
  catalog := c.Client.Catalog()

  services, _, err := catalog.Services(nil)
  if err != nil {
    panic(err)
  }

  splits := make(map[string]*Split)
  for service, tags := range services {
    if len(tags) > 0 {
      catalogServices, _, err := catalog.Service(service, "", nil)
      if err != nil {
        panic(err)
      }
      split := &Split{
        Hash: c.getHash(service),
        Services: catalogServices,
      }
      split.Spread = c.getSplitSpread(service, c.getSplitKey(service, "default"), tags)
      splits[service] = split
    }
  }

  return splits
}

func getDefaultTag(split *Split) (string, int) {
  tag := ""
  max := 0

  for k, v := range split.Spread {
    if v > max {
      max = v
      tag = k
    }
  }

  return tag, max
}

func (c *Consul) UpdateSplits(splits map[string]*Split) {
  if splits == nil {
    return
  }
  kv := c.Client.KV()
  for service, split := range splits {
    hashKey := fmt.Sprintf("split/%s/hash", service)
    log.Printf("put %s=%s", hashKey, split.Hash)
    kv.Put(&api.KVPair{ Key: hashKey, Value: []byte(split.Hash) }, nil)

    defaultTag, _ := getDefaultTag(split)
    defaultKey := fmt.Sprintf("split/%s/default", service)
    log.Printf("put %s=%s", defaultKey, defaultTag)
    kv.Put(&api.KVPair{ Key: defaultKey, Value: []byte(defaultTag) }, nil)

    kv.DeleteTree(fmt.Sprintf("split/%s/values/", service), nil)

    sum := 0
    spread := pairs.SortedFromMap(split.Spread)
    for _, pair := range spread {
      tag := pair.Key
      v := pair.Value
      log.Printf("spread %s=%v", tag, v)
      if v > 0 && tag != defaultTag {
        spreadKey := fmt.Sprintf("split/%s/values/%v", service, v + sum)
        sum += v
        log.Printf("put %s=%v", spreadKey, tag)
        kv.Put(&api.KVPair{ Key: spreadKey, Value: []byte(fmt.Sprintf("%v", tag)) }, nil)
      }
    }
  }
}
